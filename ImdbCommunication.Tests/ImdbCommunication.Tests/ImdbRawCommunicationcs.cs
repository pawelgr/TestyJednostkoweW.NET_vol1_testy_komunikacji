﻿using System.Net.Http;

namespace ImdbCommunication.Tests
{
    public class ImdbRawCommunicationcs
    {

        private const string ApiKey = "be07b217"; //todo move to configuration

        public string Search(string query)
        {
            return GetHttp("s=" + query);
        }

        public string Details(string imdbId)
        {
            return GetHttp("i=" + imdbId);
        }
        private static string GetHttp(string queryString)
        {
            string url = string.Format("http://www.omdbapi.com?apikey={0}&{1}", ApiKey, queryString);
            return new HttpClient().GetAsync(url).Result.Content.ReadAsStringAsync().Result;
        }

    }
}
