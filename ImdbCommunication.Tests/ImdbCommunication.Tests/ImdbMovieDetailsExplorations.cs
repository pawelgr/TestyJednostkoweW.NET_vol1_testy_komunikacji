﻿using System;
using System.Web.Script.Serialization;
using Xunit;

namespace ImdbCommunication.Tests
{
    public class ImdbMovieDetailsExplorations
    {
        string Execute(string imdbId)
        {
            return new ImdbRawCommunicationcs().Details(imdbId);
        }

        [Fact]
        public void ReturnsJsonResponse()
        {
            string result = Execute("tt0103776");

            Console.WriteLine(result);

            var deserialized = DeserializeJson(result);

            Assert.NotNull(deserialized);
        }

        [Fact]
        public void ReturnsDetailsAboutBatmanReturnsWhenGivenItsId()
        {
            string result = Execute("tt0103776");

            dynamic batmanReturnsDetails = DeserializeJson(result);

            Assert.Equal("Batman Returns", batmanReturnsDetails["Title"]);
            Assert.Equal("1992", batmanReturnsDetails["Year"]);
            Assert.Equal("Tim Burton", batmanReturnsDetails["Director"]);
            Assert.Equal("tt0103776", batmanReturnsDetails["imdbID"]);
            Assert.NotNull(batmanReturnsDetails["imdbRating"]);
        }


        private dynamic DeserializeJson(string json)
        {
            return new JavaScriptSerializer().Deserialize<dynamic>(json);
        }
    }


}
