﻿namespace ImdbCommunication.Tests
{
    public class Movie
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Director { get; set; }
        public string imdbID { get; set; }
        public string imdbRating { get; set; }
    }
}
