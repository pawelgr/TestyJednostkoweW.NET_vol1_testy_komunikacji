﻿using Xunit;

namespace ImdbCommunication.Tests
{
    public class ImdbDataParserParseDetailsTests
    {
        private ImdbDataParser _imdbDataParser;

        public ImdbDataParserParseDetailsTests()
        {
            _imdbDataParser = new ImdbDataParser();

        }

        Movie Execute(string json)
        {
            return _imdbDataParser.ParseDetails(json);
        }

        [Fact]
        public void ParsesMovieTitle()
        {
            Movie batmanReturnsMovie = GetBatmanReturnsMovie();

            Assert.Equal("Batman Returns", batmanReturnsMovie.Title);
        }

        [Fact]
        public void ParsesMovieYear()
        {
            Movie batmanReturnsMovie = GetBatmanReturnsMovie();
            Assert.Equal("1992", batmanReturnsMovie.Year);
        }

        [Fact]
        public void ParsesMovieDirector()
        {
            Movie batmanReturnsMovie = GetBatmanReturnsMovie();

            Assert.Equal("Tim Burton", batmanReturnsMovie.Director);
        }

        private Movie GetBatmanReturnsMovie()
        {
            return Execute(EmbeddedResources.GetResource("BatmanReturnsDetailsImdb.json"));
        }
    }
}