﻿using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace ImdbCommunication.Tests
{
    public class ImdbDataParser
    {
        public Movie ParseDetails(string json)
        {
            return new JavaScriptSerializer().Deserialize<Movie>(json);

            var deserializedObject = new JavaScriptSerializer().Deserialize<dynamic>(json);

            return new Movie()
            {
                Title = deserializedObject["Title"],
                Year = int.Parse(deserializedObject["Year"]),
                Director = deserializedObject["Director"]
            };
        }

        public IEnumerable<MovieSearchResult> ParseSearchResults(string json)
        {
            dynamic deserializedObject = new JavaScriptSerializer().Deserialize<dynamic>(json);
            dynamic[] serchResults = deserializedObject["Search"];

            var list = new List<MovieSearchResult>();
            //return null;

            foreach (dynamic dynamicSearchResult in serchResults)
            {
                list.Add(new JavaScriptSerializer().ConvertToType<MovieSearchResult>(dynamicSearchResult));
            }

            return list;
        }
    }
}