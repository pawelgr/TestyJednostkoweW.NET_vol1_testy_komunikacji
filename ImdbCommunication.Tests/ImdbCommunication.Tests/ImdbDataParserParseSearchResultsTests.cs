﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ImdbCommunication.Tests
{
    public class ImdbDataParserParseSearchResultsTests
    {
        private ImdbDataParser _imdbDataParser;

        public ImdbDataParserParseSearchResultsTests()
        {
            _imdbDataParser = new ImdbDataParser();
        }

        IEnumerable<MovieSearchResult> execute(string json)
        {
            return _imdbDataParser.ParseSearchResults(json);
        }
        public void ParsesMovieTitle()
        {
            IEnumerable<MovieSearchResult> batmanSearchResults = GetMovieSearchResultsByBatman();

            Assert.True(batmanSearchResults.ToArray().Length > 1);

            MovieSearchResult batmanReturns = batmanSearchResults.SingleOrDefault(x => x.Title == "Batman Returns");

            Assert.Equal("Batman Returns", batmanReturns.Title);
        }

        [Fact]
        public void ParsesMovieYear()
        {
            IEnumerable<MovieSearchResult> batmanSearchResults = GetMovieSearchResultsByBatman();

            Assert.True(batmanSearchResults.ToArray().Length > 1);

            MovieSearchResult batmanReturns = batmanSearchResults.SingleOrDefault(x => x.Title == "Batman Returns");

            Assert.Equal("1992", batmanReturns.Year);
        }

        [Fact]
        public void ParsesMovieImdbId()
        {
            IEnumerable<MovieSearchResult> batmanSearchResults = GetMovieSearchResultsByBatman();
            Assert.True(batmanSearchResults.ToArray().Length > 1);

            MovieSearchResult batmanReturns = batmanSearchResults.SingleOrDefault(x => x.Title == "Batman Returns");
            Assert.Equal("tt0103776", batmanReturns.ImdbId);
        }
        private IEnumerable<MovieSearchResult> GetMovieSearchResultsByBatman()
        {
            return execute(EmbeddedResources.GetResource("BatmanSearchResultsImdb.json"));
        }
    }
}