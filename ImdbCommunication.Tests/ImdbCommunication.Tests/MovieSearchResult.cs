﻿using System.Collections.Generic;

namespace ImdbCommunication.Tests
{
    public class MovieSearchResult
    {
        public string Poster { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Year { get; set; }
        public string ImdbId { get; set; }
    }
}
