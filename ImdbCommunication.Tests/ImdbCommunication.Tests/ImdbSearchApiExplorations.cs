﻿using System;
using System.Linq;
using System.Web.Script.Serialization;
using Xunit;

namespace ImdbCommunication.Tests
{
    public class ImdbSearchApiExplorations
    {
        //OMDb API: http://www.omdbapi.com/?i=tt3896198&apikey=be07b217
        //Vog https://www.youtube.com/watch?v=gQaShMN_tN8

            //Ctrl+R+V introduce variable

            //Ctrl+R+F create varable - okno z opcjami


        string Execute(string query)
        {
            return new ImdbRawCommunicationcs().Search(query);
        }

        [Fact]
        public void ReturnsResponse()
        {
            string results = Execute("Batman");

            Console.WriteLine(results);

            Assert.NotEmpty(results);
        }

        [Fact]
        public void ReturnsJsonResponse()
        {
            string results = Execute("Batman");

            var deserialized = DeserializeJson(results);

            Assert.NotNull(deserialized);
        }

        [Fact]
        public void ReturnBatmanReturnsAmongOtherResultsWhenSerchingForBatman()
        {
            string json = Execute("Batman");
            dynamic deserializedObject = DeserializeJson(json);
            dynamic[] serchResults = deserializedObject["Search"];

            Assert.True(serchResults.Length > 1);

            dynamic batmanReturns = serchResults.SingleOrDefault(x => x["Title"] == "Batman Returns");

            Assert.NotNull(batmanReturns);

            Assert.Equal("Batman Returns", batmanReturns["Title"]);
            Assert.Equal("1992", batmanReturns["Year"]);
            Assert.Equal("tt0103776", batmanReturns["imdbID"]);
        }

        private dynamic DeserializeJson(string json)
        {
            return new JavaScriptSerializer().Deserialize<dynamic>(json);
        }

        //[Fact]
        //public void FactMethodName()
        //{
        //    // Fazy testów
        //    // 1) arrange

        //    // 2) act
        //    //Execute();

        //    // 3) assert
        //}


    }
}
